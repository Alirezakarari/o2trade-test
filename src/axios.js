import axios from 'axios'
import router from "./router.js";
const ax = axios.create({
    baseURL: 'https://api.o2trade.ir/api/v1/',
    headers:{
        'Content-Type' :'application/json',
        'accept'       :'application/json',
        'Authorization':'Bearer ' + localStorage.getItem("token") || localStorage['token']
    },
})

ax.interceptors.request.use(function (config) {
    config.headers = {
        'Content-Type': 'application/json',
        'accept':'application/json'
    };
    if (localStorage.getItem("token")) {
        config.headers['Authorization'] = 'Bearer ' + localStorage.getItem("token");
    }
    return config;
}, function (error) {
    return Promise.reject(error);
});

ax.interceptors.response.use(function (res) {
    return res;
}, function (e) {
    if (e?.response?.status === 401) {
        router.push({path:'/'});
    }
    return Promise.reject(e);
});


export default ax