import { createApp } from 'vue'
import * as bootstrap from 'bootstrap';
import App from './App.vue'
import './style.css'
import 'bootstrap/scss/bootstrap.scss'
import router from "./router.js";
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';


createApp(App).use(router).use(VueSweetalert2).mount('#app')
