import router from "../router.js";
import axios from "../axios.js";
export const redirect = async (name) => {
    await router.push({name: name})
}
export const checkLogin = async () => {
    try {
        const {data} = await axios.post('/checkLogin')
        return data
    }catch (e) {
        throw e
    }
}
