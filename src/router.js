import { createRouter, createWebHistory } from 'vue-router'
import Login from './components/Login.vue'
import {checkLogin} from './utils/utils'
const router =  createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            name: 'login',
            component: Login,
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: () => import('./components/Dashboard.vue'),
        },
        {
            path: '/register',
            name: 'register',
            component: () => import('./components/Register.vue'),
        },
        {
            path: "/:pathMatch(.*)*",
            name: '404',
            component: () => import('./components/404.vue'),
        },
    ],
})

const nonRoutesCheckLogin = [
    '/',
    '/register',
]

router.beforeEach((to, from, next) => {
    if (nonRoutesCheckLogin.includes(to.path)) {
        return next()
    }else {
        if (checkLogin()) next()
    }
})

export default router